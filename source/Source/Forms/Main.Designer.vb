﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.stripMenu = New System.Windows.Forms.ToolStrip()
        Me.stripMenu_File = New System.Windows.Forms.ToolStripDropDownButton()
        Me.stripMenu_File_New = New System.Windows.Forms.ToolStripMenuItem()
        Me.stripMenu_File_Load = New System.Windows.Forms.ToolStripMenuItem()
        Me.stripMenu_File_Save = New System.Windows.Forms.ToolStripMenuItem()
        Me.stripMenu_File_SaveAs = New System.Windows.Forms.ToolStripMenuItem()
        Me.stripMenu_File_Delete = New System.Windows.Forms.ToolStripMenuItem()
        Me.stripMenu_Edit = New System.Windows.Forms.ToolStripDropDownButton()
        Me.stripMenu_Edit_Fill = New System.Windows.Forms.ToolStripMenuItem()
        Me.stripMenu_Edit_Clear = New System.Windows.Forms.ToolStripMenuItem()
        Me.dockTileset = New System.Windows.Forms.Panel()
        Me.pnlTileset = New System.Windows.Forms.Panel()
        Me.rsTileset = New SFML.UI.SFControl
        Me.scrlTilesetChange = New System.Windows.Forms.HScrollBar()
        Me.pnlSolution = New System.Windows.Forms.Panel()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.lstIndex = New System.Windows.Forms.ListBox()
        Me.scrlEditLayer = New System.Windows.Forms.HScrollBar()
        Me.btnProperties = New System.Windows.Forms.Button()
        Me.scrlEditZ = New System.Windows.Forms.HScrollBar()
        Me.scrlSizeZ = New System.Windows.Forms.HScrollBar()
        Me.scrlSizeY = New System.Windows.Forms.HScrollBar()
        Me.scrlSizeX = New System.Windows.Forms.HScrollBar()
        Me.lblSizeX = New System.Windows.Forms.Label()
        Me.btnShiftRight = New System.Windows.Forms.Button()
        Me.btnShiftLeft = New System.Windows.Forms.Button()
        Me.btnShiftDown = New System.Windows.Forms.Button()
        Me.btnShiftUp = New System.Windows.Forms.Button()
        Me.ofdBox = New System.Windows.Forms.OpenFileDialog()
        Me.dockMap = New System.Windows.Forms.Panel()
        Me.rsMap = New SFML.UI.SFControl
        Me.pnlProperties = New System.Windows.Forms.Panel()
        Me.gbResizeMap = New System.Windows.Forms.GroupBox()
        Me.lblSizeZ = New System.Windows.Forms.Label()
        Me.lblSizeY = New System.Windows.Forms.Label()
        Me.gbEditingLayer = New System.Windows.Forms.GroupBox()
        Me.lblEditZ = New System.Windows.Forms.Label()
        Me.lblEditLayer = New System.Windows.Forms.Label()
        Me.gbShiftMap = New System.Windows.Forms.GroupBox()
        Me.splitMain = New System.Windows.Forms.SplitContainer()
        Me.stripMenu.SuspendLayout
        Me.dockTileset.SuspendLayout
        Me.pnlTileset.SuspendLayout
        Me.pnlSolution.SuspendLayout
        Me.dockMap.SuspendLayout
        Me.pnlProperties.SuspendLayout
        Me.gbResizeMap.SuspendLayout
        Me.gbEditingLayer.SuspendLayout
        Me.gbShiftMap.SuspendLayout
        CType(Me.splitMain, System.ComponentModel.ISupportInitialize).BeginInit
        Me.splitMain.Panel1.SuspendLayout
        Me.splitMain.Panel2.SuspendLayout
        Me.splitMain.SuspendLayout
        Me.SuspendLayout
        '
        'stripMenu
        '
        Me.stripMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.stripMenu_File, Me.stripMenu_Edit})
        Me.stripMenu.Location = New System.Drawing.Point(0, 0)
        Me.stripMenu.Name = "stripMenu"
        Me.stripMenu.Size = New System.Drawing.Size(848, 25)
        Me.stripMenu.TabIndex = 0
        '
        'stripMenu_File
        '
        Me.stripMenu_File.AutoToolTip = False
        Me.stripMenu_File.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.stripMenu_File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.stripMenu_File_New, Me.stripMenu_File_Load, Me.stripMenu_File_Save, Me.stripMenu_File_SaveAs, Me.stripMenu_File_Delete})
        Me.stripMenu_File.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.stripMenu_File.Name = "stripMenu_File"
        Me.stripMenu_File.Size = New System.Drawing.Size(38, 22)
        Me.stripMenu_File.Text = "File"
        '
        'stripMenu_File_New
        '
        Me.stripMenu_File_New.Name = "stripMenu_File_New"
        Me.stripMenu_File_New.Size = New System.Drawing.Size(112, 22)
        Me.stripMenu_File_New.Text = "New"
        '
        'stripMenu_File_Load
        '
        Me.stripMenu_File_Load.Name = "stripMenu_File_Load"
        Me.stripMenu_File_Load.Size = New System.Drawing.Size(112, 22)
        Me.stripMenu_File_Load.Text = "Load"
        '
        'stripMenu_File_Save
        '
        Me.stripMenu_File_Save.Name = "stripMenu_File_Save"
        Me.stripMenu_File_Save.Size = New System.Drawing.Size(112, 22)
        Me.stripMenu_File_Save.Text = "Save"
        '
        'stripMenu_File_SaveAs
        '
        Me.stripMenu_File_SaveAs.Name = "stripMenu_File_SaveAs"
        Me.stripMenu_File_SaveAs.Size = New System.Drawing.Size(112, 22)
        Me.stripMenu_File_SaveAs.Text = "Save As"
        '
        'stripMenu_File_Delete
        '
        Me.stripMenu_File_Delete.Name = "stripMenu_File_Delete"
        Me.stripMenu_File_Delete.Size = New System.Drawing.Size(112, 22)
        Me.stripMenu_File_Delete.Text = "Delete"
        '
        'stripMenu_Edit
        '
        Me.stripMenu_Edit.AutoToolTip = False
        Me.stripMenu_Edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.stripMenu_Edit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.stripMenu_Edit_Fill, Me.stripMenu_Edit_Clear})
        Me.stripMenu_Edit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.stripMenu_Edit.Name = "stripMenu_Edit"
        Me.stripMenu_Edit.Size = New System.Drawing.Size(40, 22)
        Me.stripMenu_Edit.Text = "Edit"
        '
        'stripMenu_Edit_Fill
        '
        Me.stripMenu_Edit_Fill.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.stripMenu_Edit_Fill.Name = "stripMenu_Edit_Fill"
        Me.stripMenu_Edit_Fill.Size = New System.Drawing.Size(100, 22)
        Me.stripMenu_Edit_Fill.Text = "Fill"
        '
        'stripMenu_Edit_Clear
        '
        Me.stripMenu_Edit_Clear.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.stripMenu_Edit_Clear.Name = "stripMenu_Edit_Clear"
        Me.stripMenu_Edit_Clear.Size = New System.Drawing.Size(100, 22)
        Me.stripMenu_Edit_Clear.Text = "Clear"
        '
        'dockTileset
        '
        Me.dockTileset.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.dockTileset.Controls.Add(Me.pnlTileset)
        Me.dockTileset.Controls.Add(Me.scrlTilesetChange)
        Me.dockTileset.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dockTileset.Location = New System.Drawing.Point(0, 0)
        Me.dockTileset.Name = "dockTileset"
        Me.dockTileset.Size = New System.Drawing.Size(242, 379)
        Me.dockTileset.TabIndex = 3
        '
        'pnlTileset
        '
        Me.pnlTileset.AutoScroll = True
        Me.pnlTileset.Controls.Add(Me.rsTileset)
        Me.pnlTileset.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTileset.Location = New System.Drawing.Point(0, 0)
        Me.pnlTileset.Name = "pnlTileset"
        Me.pnlTileset.Size = New System.Drawing.Size(242, 362)
        Me.pnlTileset.TabIndex = 23
        '
        'rsTileset
        '
        Me.rsTileset.AutoDraw = True
        Me.rsTileset.AutoDrawInterval = 30
        Me.rsTileset.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.rsTileset.Location = New System.Drawing.Point(0, 0)
        Me.rsTileset.Name = "rsTileset"
        Me.rsTileset.Size = New System.Drawing.Size(32, 32)
        Me.rsTileset.TabIndex = 1
        '
        'scrlTilesetChange
        '
        Me.scrlTilesetChange.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.scrlTilesetChange.LargeChange = 1
        Me.scrlTilesetChange.Location = New System.Drawing.Point(0, 362)
        Me.scrlTilesetChange.Maximum = 10
        Me.scrlTilesetChange.Minimum = 1
        Me.scrlTilesetChange.Name = "scrlTilesetChange"
        Me.scrlTilesetChange.Size = New System.Drawing.Size(242, 17)
        Me.scrlTilesetChange.TabIndex = 21
        Me.scrlTilesetChange.Value = 1
        '
        'pnlSolution
        '
        Me.pnlSolution.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlSolution.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.pnlSolution.Controls.Add(Me.btnSearch)
        Me.pnlSolution.Controls.Add(Me.txtSearch)
        Me.pnlSolution.Controls.Add(Me.lstIndex)
        Me.pnlSolution.Location = New System.Drawing.Point(3, 3)
        Me.pnlSolution.Name = "pnlSolution"
        Me.pnlSolution.Size = New System.Drawing.Size(220, 117)
        Me.pnlSolution.TabIndex = 16
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearch.Location = New System.Drawing.Point(197, 93)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(20, 20)
        Me.btnSearch.TabIndex = 7
        Me.btnSearch.Text = "?"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'txtSearch
        '
        Me.txtSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearch.Location = New System.Drawing.Point(3, 93)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(188, 20)
        Me.txtSearch.TabIndex = 6
        '
        'lstIndex
        '
        Me.lstIndex.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstIndex.FormattingEnabled = True
        Me.lstIndex.IntegralHeight = False
        Me.lstIndex.ItemHeight = 14
        Me.lstIndex.Location = New System.Drawing.Point(0, 0)
        Me.lstIndex.Name = "lstIndex"
        Me.lstIndex.ScrollAlwaysVisible = True
        Me.lstIndex.Size = New System.Drawing.Size(220, 87)
        Me.lstIndex.Sorted = True
        Me.lstIndex.TabIndex = 5
        '
        'scrlEditLayer
        '
        Me.scrlEditLayer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.scrlEditLayer.LargeChange = 1
        Me.scrlEditLayer.Location = New System.Drawing.Point(109, 16)
        Me.scrlEditLayer.Maximum = 1
        Me.scrlEditLayer.Name = "scrlEditLayer"
        Me.scrlEditLayer.Size = New System.Drawing.Size(99, 15)
        Me.scrlEditLayer.TabIndex = 38
        '
        'btnProperties
        '
        Me.btnProperties.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProperties.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnProperties.Location = New System.Drawing.Point(131, 19)
        Me.btnProperties.Name = "btnProperties"
        Me.btnProperties.Size = New System.Drawing.Size(77, 78)
        Me.btnProperties.TabIndex = 36
        Me.btnProperties.Text = "Properties"
        Me.btnProperties.UseVisualStyleBackColor = True
        '
        'scrlEditZ
        '
        Me.scrlEditZ.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.scrlEditZ.LargeChange = 1
        Me.scrlEditZ.Location = New System.Drawing.Point(109, 35)
        Me.scrlEditZ.Maximum = 1
        Me.scrlEditZ.Name = "scrlEditZ"
        Me.scrlEditZ.Size = New System.Drawing.Size(99, 15)
        Me.scrlEditZ.TabIndex = 34
        '
        'scrlSizeZ
        '
        Me.scrlSizeZ.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.scrlSizeZ.LargeChange = 1
        Me.scrlSizeZ.Location = New System.Drawing.Point(69, 54)
        Me.scrlSizeZ.Maximum = 5
        Me.scrlSizeZ.Name = "scrlSizeZ"
        Me.scrlSizeZ.Size = New System.Drawing.Size(139, 15)
        Me.scrlSizeZ.TabIndex = 32
        '
        'scrlSizeY
        '
        Me.scrlSizeY.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.scrlSizeY.LargeChange = 1
        Me.scrlSizeY.Location = New System.Drawing.Point(69, 35)
        Me.scrlSizeY.Maximum = 265
        Me.scrlSizeY.Name = "scrlSizeY"
        Me.scrlSizeY.Size = New System.Drawing.Size(139, 15)
        Me.scrlSizeY.TabIndex = 30
        Me.scrlSizeY.Value = 15
        '
        'scrlSizeX
        '
        Me.scrlSizeX.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.scrlSizeX.LargeChange = 1
        Me.scrlSizeX.Location = New System.Drawing.Point(69, 16)
        Me.scrlSizeX.Maximum = 265
        Me.scrlSizeX.Name = "scrlSizeX"
        Me.scrlSizeX.Size = New System.Drawing.Size(139, 15)
        Me.scrlSizeX.TabIndex = 29
        Me.scrlSizeX.Value = 15
        '
        'lblSizeX
        '
        Me.lblSizeX.Location = New System.Drawing.Point(6, 16)
        Me.lblSizeX.Name = "lblSizeX"
        Me.lblSizeX.Size = New System.Drawing.Size(60, 15)
        Me.lblSizeX.TabIndex = 27
        Me.lblSizeX.Text = "X: 16"
        Me.lblSizeX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnShiftRight
        '
        Me.btnShiftRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnShiftRight.Location = New System.Drawing.Point(70, 47)
        Me.btnShiftRight.Name = "btnShiftRight"
        Me.btnShiftRight.Size = New System.Drawing.Size(55, 22)
        Me.btnShiftRight.TabIndex = 24
        Me.btnShiftRight.Text = "Right"
        Me.btnShiftRight.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnShiftRight.UseVisualStyleBackColor = True
        '
        'btnShiftLeft
        '
        Me.btnShiftLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnShiftLeft.Location = New System.Drawing.Point(9, 47)
        Me.btnShiftLeft.Name = "btnShiftLeft"
        Me.btnShiftLeft.Size = New System.Drawing.Size(55, 22)
        Me.btnShiftLeft.TabIndex = 23
        Me.btnShiftLeft.Text = "Left"
        Me.btnShiftLeft.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnShiftLeft.UseVisualStyleBackColor = True
        '
        'btnShiftDown
        '
        Me.btnShiftDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnShiftDown.Location = New System.Drawing.Point(41, 75)
        Me.btnShiftDown.Name = "btnShiftDown"
        Me.btnShiftDown.Size = New System.Drawing.Size(55, 22)
        Me.btnShiftDown.TabIndex = 22
        Me.btnShiftDown.Text = "Down"
        Me.btnShiftDown.UseVisualStyleBackColor = True
        '
        'btnShiftUp
        '
        Me.btnShiftUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnShiftUp.Location = New System.Drawing.Point(41, 19)
        Me.btnShiftUp.Name = "btnShiftUp"
        Me.btnShiftUp.Size = New System.Drawing.Size(55, 22)
        Me.btnShiftUp.TabIndex = 21
        Me.btnShiftUp.Text = "Up"
        Me.btnShiftUp.UseVisualStyleBackColor = True
        '
        'dockMap
        '
        Me.dockMap.AutoScroll = True
        Me.dockMap.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.dockMap.Controls.Add(Me.rsMap)
        Me.dockMap.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dockMap.Location = New System.Drawing.Point(0, 0)
        Me.dockMap.Name = "dockMap"
        Me.dockMap.Size = New System.Drawing.Size(368, 379)
        Me.dockMap.TabIndex = 19
        '
        'rsMap
        '
        Me.rsMap.AutoDraw = True
        Me.rsMap.AutoDrawInterval = 30
        Me.rsMap.Location = New System.Drawing.Point(0, 0)
        Me.rsMap.Name = "rsMap"
        Me.rsMap.Size = New System.Drawing.Size(32, 32)
        Me.rsMap.TabIndex = 0
        '
        'pnlProperties
        '
        Me.pnlProperties.Controls.Add(Me.pnlSolution)
        Me.pnlProperties.Controls.Add(Me.gbResizeMap)
        Me.pnlProperties.Controls.Add(Me.gbEditingLayer)
        Me.pnlProperties.Controls.Add(Me.gbShiftMap)
        Me.pnlProperties.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlProperties.Location = New System.Drawing.Point(0, 25)
        Me.pnlProperties.Name = "pnlProperties"
        Me.pnlProperties.Size = New System.Drawing.Size(226, 383)
        Me.pnlProperties.TabIndex = 20
        '
        'gbResizeMap
        '
        Me.gbResizeMap.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbResizeMap.Controls.Add(Me.lblSizeZ)
        Me.gbResizeMap.Controls.Add(Me.lblSizeY)
        Me.gbResizeMap.Controls.Add(Me.lblSizeX)
        Me.gbResizeMap.Controls.Add(Me.scrlSizeX)
        Me.gbResizeMap.Controls.Add(Me.scrlSizeY)
        Me.gbResizeMap.Controls.Add(Me.scrlSizeZ)
        Me.gbResizeMap.Location = New System.Drawing.Point(3, 126)
        Me.gbResizeMap.Name = "gbResizeMap"
        Me.gbResizeMap.Size = New System.Drawing.Size(220, 76)
        Me.gbResizeMap.TabIndex = 0
        Me.gbResizeMap.TabStop = False
        Me.gbResizeMap.Text = "Resize Map"
        '
        'lblSizeZ
        '
        Me.lblSizeZ.Location = New System.Drawing.Point(6, 54)
        Me.lblSizeZ.Name = "lblSizeZ"
        Me.lblSizeZ.Size = New System.Drawing.Size(60, 15)
        Me.lblSizeZ.TabIndex = 29
        Me.lblSizeZ.Text = "Z: 1"
        Me.lblSizeZ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSizeY
        '
        Me.lblSizeY.Location = New System.Drawing.Point(6, 35)
        Me.lblSizeY.Name = "lblSizeY"
        Me.lblSizeY.Size = New System.Drawing.Size(60, 15)
        Me.lblSizeY.TabIndex = 28
        Me.lblSizeY.Text = "Y: 16"
        Me.lblSizeY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEditingLayer
        '
        Me.gbEditingLayer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbEditingLayer.Controls.Add(Me.scrlEditLayer)
        Me.gbEditingLayer.Controls.Add(Me.lblEditZ)
        Me.gbEditingLayer.Controls.Add(Me.scrlEditZ)
        Me.gbEditingLayer.Controls.Add(Me.lblEditLayer)
        Me.gbEditingLayer.Location = New System.Drawing.Point(3, 208)
        Me.gbEditingLayer.Name = "gbEditingLayer"
        Me.gbEditingLayer.Size = New System.Drawing.Size(220, 58)
        Me.gbEditingLayer.TabIndex = 29
        Me.gbEditingLayer.TabStop = False
        Me.gbEditingLayer.Text = "Editing Layer"
        '
        'lblEditZ
        '
        Me.lblEditZ.Location = New System.Drawing.Point(6, 35)
        Me.lblEditZ.Name = "lblEditZ"
        Me.lblEditZ.Size = New System.Drawing.Size(100, 15)
        Me.lblEditZ.TabIndex = 30
        Me.lblEditZ.Text = "Z: 1"
        Me.lblEditZ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEditLayer
        '
        Me.lblEditLayer.Location = New System.Drawing.Point(6, 16)
        Me.lblEditLayer.Name = "lblEditLayer"
        Me.lblEditLayer.Size = New System.Drawing.Size(100, 15)
        Me.lblEditLayer.TabIndex = 28
        Me.lblEditLayer.Text = "Layer: Ground"
        Me.lblEditLayer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbShiftMap
        '
        Me.gbShiftMap.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbShiftMap.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbShiftMap.Controls.Add(Me.btnProperties)
        Me.gbShiftMap.Controls.Add(Me.btnShiftUp)
        Me.gbShiftMap.Controls.Add(Me.btnShiftDown)
        Me.gbShiftMap.Controls.Add(Me.btnShiftRight)
        Me.gbShiftMap.Controls.Add(Me.btnShiftLeft)
        Me.gbShiftMap.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbShiftMap.Location = New System.Drawing.Point(3, 272)
        Me.gbShiftMap.Name = "gbShiftMap"
        Me.gbShiftMap.Size = New System.Drawing.Size(220, 108)
        Me.gbShiftMap.TabIndex = 25
        Me.gbShiftMap.TabStop = False
        Me.gbShiftMap.Text = "Shift Map"
        '
        'splitMain
        '
        Me.splitMain.BackColor = System.Drawing.SystemColors.Control
        Me.splitMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.splitMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitMain.Location = New System.Drawing.Point(226, 25)
        Me.splitMain.Name = "splitMain"
        '
        'splitMain.Panel1
        '
        Me.splitMain.Panel1.Controls.Add(Me.dockMap)
        '
        'splitMain.Panel2
        '
        Me.splitMain.Panel2.Controls.Add(Me.dockTileset)
        Me.splitMain.Size = New System.Drawing.Size(622, 383)
        Me.splitMain.SplitterDistance = 372
        Me.splitMain.TabIndex = 21
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 14!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(848, 408)
        Me.Controls.Add(Me.splitMain)
        Me.Controls.Add(Me.pnlProperties)
        Me.Controls.Add(Me.stripMenu)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(860, 440)
        Me.Name = "frmMain"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Map Editor"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.stripMenu.ResumeLayout(False)
        Me.stripMenu.PerformLayout
        Me.dockTileset.ResumeLayout(False)
        Me.pnlTileset.ResumeLayout(False)
        Me.pnlSolution.ResumeLayout(False)
        Me.pnlSolution.PerformLayout
        Me.dockMap.ResumeLayout(False)
        Me.pnlProperties.ResumeLayout(False)
        Me.gbResizeMap.ResumeLayout(False)
        Me.gbEditingLayer.ResumeLayout(False)
        Me.gbShiftMap.ResumeLayout(False)
        Me.splitMain.Panel1.ResumeLayout(False)
        Me.splitMain.Panel2.ResumeLayout(False)
        CType(Me.splitMain, System.ComponentModel.ISupportInitialize).EndInit
        Me.splitMain.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout

    End Sub
    Private dockMap As System.Windows.Forms.Panel
    Friend WithEvents stripMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents dockTileset As System.Windows.Forms.Panel
    Friend WithEvents stripMenu_Edit As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents pnlSolution As System.Windows.Forms.Panel
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents lstIndex As System.Windows.Forms.ListBox
    Friend WithEvents stripMenu_Edit_Fill As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stripMenu_Edit_Clear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents scrlSizeZ As System.Windows.Forms.HScrollBar
    Friend WithEvents scrlSizeY As System.Windows.Forms.HScrollBar
    Friend WithEvents scrlSizeX As System.Windows.Forms.HScrollBar
    Friend WithEvents lblSizeX As System.Windows.Forms.Label
    Friend WithEvents btnShiftRight As System.Windows.Forms.Button
    Friend WithEvents btnShiftLeft As System.Windows.Forms.Button
    Friend WithEvents btnShiftDown As System.Windows.Forms.Button
    Friend WithEvents btnShiftUp As System.Windows.Forms.Button
    Friend WithEvents scrlEditZ As System.Windows.Forms.HScrollBar
    Friend WithEvents scrlTilesetChange As System.Windows.Forms.HScrollBar
    Friend WithEvents btnProperties As System.Windows.Forms.Button
    Friend WithEvents scrlEditLayer As System.Windows.Forms.HScrollBar
    Friend WithEvents ofdBox As System.Windows.Forms.OpenFileDialog
    Friend WithEvents stripMenu_File As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents stripMenu_File_New As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stripMenu_File_Load As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stripMenu_File_Save As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stripMenu_File_Delete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stripMenu_File_SaveAs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlProperties As Panel
    Friend WithEvents gbResizeMap As GroupBox
    Friend WithEvents lblSizeZ As Label
    Friend WithEvents lblSizeY As Label
    Friend WithEvents gbEditingLayer As GroupBox
    Friend WithEvents lblEditZ As Label
    Friend WithEvents lblEditLayer As Label
    Friend WithEvents gbShiftMap As GroupBox
    Friend WithEvents pnlTileset As Panel
    Friend WithEvents rsTileset As SFML.UI.SFControl
    Friend WithEvents rsMap As SFML.UI.SFControl
    Friend WithEvents splitMain As SplitContainer
End Class
