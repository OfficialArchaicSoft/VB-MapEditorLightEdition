﻿Imports System
Imports System.IO

Friend Class frmProperties
    Private Sub frmProperties_VisibleChanged(sender As Object, e As EventArgs) Handles Me.VisibleChanged

        If Not Visible Then Exit Sub

        Dim cd As New DirectoryInfo(Path.Music)
        Dim fi As FileInfo()

        cmbMusic.Items.Clear() : cmbMusic.Items.Add("")

        fi = cd.GetFiles("*.ogg")
        For Each f As FileInfo In fi : cmbMusic.Items.Add(f.Name) : Next

        fi = cd.GetFiles("*.flac")
        For Each f As FileInfo In fi : cmbMusic.Items.Add(f.Name) : Next

        Audio.StopMusic()

        txtName.Text = Map.Name
        For i As Integer = 0 To cmbMusic.Items.Count - 1
            If cmbMusic.Items.Item(i).ToString = Map.Music Then
                cmbMusic.SelectedIndex = i
            End If
        Next

        txtComments.Text = Map.Comment

    End Sub

    Private Sub btnPlay_Click(sender As Object, e As EventArgs) Handles btnPlay.Click

        Audio.PlayMusic(cmbMusic.Items.Item(cmbMusic.SelectedIndex).ToString)

    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click

        Audio.StopMusic()

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Map.Name = txtName.Text
        Map.Music = cmbMusic.Items.Item(cmbMusic.SelectedIndex).ToString
        Map.Comment = txtComments.Text
        MapModified = True : Me.Hide()

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        Hide()

    End Sub
End Class