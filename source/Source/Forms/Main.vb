﻿Imports System
Imports System.IO
Imports System.Windows.Forms
Imports SFML.Graphics
Imports SFML.System

Partial Friend Class frmMain

#Region " Form "

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ' Prevent loading if no tilesets
        If Graphics.Tileset.GetUpperBound(0) < 2 Then Environment.Exit(0)

        ResetPropertyWindow()
        RelistMaps()

        scrlTilesetChange.Maximum = Graphics.Tileset.GetUpperBound(0)
        scrlTilesetChange_Scroll(sender, Nothing)

        scrlEditLayer.Maximum = LayerType.Count - 1

        If lstIndex.Items.Count > 0 Then
            lstIndex.SelectedIndex = 0
            lstIndex_DoubleClick(sender, e)
        End If

    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing

        If MapModified Then

            Dim response As DialogResult = MessageBox.Show(
                "You have unsaved changes, are you sure wish to exit?",
                "Warning: Loss of progress!",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Warning)

            If response = DialogResult.Yes Then

                Terminate()

            ElseIf response = DialogResult.No Then

                response = MessageBox.Show("Would you like to save then exit?",
                                           "Save (Optionally)",
                                           MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Question)

                If response = DialogResult.Yes Then
                    stripMenu_File_Save_Click(sender, e)
                    Terminate()
                End If

            End If

        Else

            Terminate()

        End If

        ' They chose not to exit
        e.Cancel = True
        Exit Sub

    End Sub

#End Region

#Region " Menu Strip "

    Private Sub stripMenu_File_New_Click(sender As Object, e As EventArgs) Handles stripMenu_File_New.Click

        If MapModified Then

            Dim response As DialogResult = MessageBox.Show(
                "You have unsaved changes, would you like to save before creating a new map?",
                "Warning: Loss of progress!",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Warning)

            If response = DialogResult.Yes Then

                stripMenu_File_Save_Click(sender, e)

            ElseIf response = DialogResult.No Then

                response = MessageBox.Show(
                    "You are about to lose progress, are you sure you wish to continue?",
                    "Warning: Loss of progress!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question)

                If response = DialogResult.No Then Exit Sub

            Else ' They chose cancel

                Exit Sub

            End If

        End If

        Audio.StopMusic()
        Map = New MapDef(16, 16, 1)

        Dim name As String = Microsoft.VisualBasic.InputBox("Enter a name for your new map...", "New Map")

        If name.Trim.Length < 1 Then Exit Sub

        If File.Exists(Path.Map & name & ".map") Then

            If MessageBox.Show("Map with that name already exists. Would you like to overwrite it?",
                               "Chosen Map Name: " & name,
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Hand) = DialogResult.No Then Return

        End If

        Map.Name = name
        ResetPropertyWindow()
        stripMenu_File_Save_Click(sender, e)

    End Sub

    Private Sub stripMenu_File_Load_Click(sender As Object, e As EventArgs) Handles stripMenu_File_Load.Click

        If MapModified Then

            Dim response As DialogResult = MessageBox.Show(
                "You have unsaved changes, would you like to save before changing maps?",
                "Warning: Loss of progress!",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Warning)

            If response = DialogResult.Yes Then

                stripMenu_File_Save_Click(sender, e)

            ElseIf response = DialogResult.No Then

                response = MessageBox.Show(
                    "You are about to lose progress, are you sure you wish to continue?",
                    "Warning: Loss of progress!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question)

                If response = DialogResult.No Then Exit Sub

            Else ' They chose cancel

                Exit Sub

            End If

        End If

        Audio.StopMusic()
        ofdBox.InitialDirectory = Path.Map
        ofdBox.ShowDialog()
        Map.Load(ofdBox.FileName)
        ResetPropertyWindow()
        MapModified = False

    End Sub

    Private Sub stripMenu_File_Save_Click(sender As Object, e As EventArgs) Handles stripMenu_File_Save.Click

        If Not Map.Name.Trim.Length > 0 Then

            Dim response As DialogResult = MessageBox.Show(
                "Map name is empty or invalid. Would you like to enter one now?",
                "Yes to Enter name, No to Cancel without Saving.",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning)

            If response = DialogResult.Yes Then

                stripMenu_File_SaveAs_Click(sender, e)
                Exit Sub

            Else

                Exit Sub

            End If

        End If

        Dim cf As String = Path.Map & Map.Name & ".map"

        Map.Revision = If(Not File.Exists(cf), 0, Map.Revision + 1)
        Map.Save(cf)
        RelistMaps()
        MapModified = False

    End Sub

    Private Sub stripMenu_File_SaveAs_Click(sender As Object, e As EventArgs) Handles stripMenu_File_SaveAs.Click

        Dim name As String = Microsoft.VisualBasic.InputBox("New map name =", "Save As")

        If Not name.Trim.Length > 0 Then Return

        Map.Name = name
        stripMenu_File_Save_Click(sender, e)

    End Sub

    Private Sub stripMenu_File_Delete_Click(sender As Object, e As EventArgs) Handles stripMenu_File_Delete.Click

        If File.Exists(Path.Map & Map.Name & ".map") Then
            File.Delete(Path.Map & Map.Name & ".map")
        End If

        RelistMaps()
        MapModified = False

        If lstIndex.Items.Count > 0 Then

            lstIndex.SelectedIndex = 0
            lstIndex_DoubleClick(sender, e)

        Else

            stripMenu_File_New_Click(sender, e)

        End If

    End Sub

    Private Sub stripMenu_Edit_Fill_Click(sender As Object, e As EventArgs) Handles stripMenu_Edit_Fill.Click

        For y As Integer = 0 To Map.MaxY

            For x As Integer = 0 To Map.MaxX

                SetTile(x, y)

            Next

        Next

    End Sub

    Private Sub stripMenu_Edit_Clear_Click(sender As Object, e As EventArgs) Handles stripMenu_Edit_Clear.Click

        For y As Integer = 0 To Map.MaxY

            For x As Integer = 0 To Map.MaxX

                ClearTile(x, y)

            Next

        Next

    End Sub

#End Region

#Region " Solution Explorer "

    Private Sub lstIndex_DoubleClick(sender As Object, e As EventArgs) Handles lstIndex.DoubleClick

        If lstIndex.SelectedItem Is Nothing Then Exit Sub
        If lstIndex.SelectedItem.ToString.Trim.Length <= 0 Then Exit Sub

        If MapModified Then

            Dim response As DialogResult = MessageBox.Show(
                "You have unsaved changes, would you like to save before changing maps?",
                "Warning: Loss of progress!",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Warning)

            If response = DialogResult.Yes Then

                Dim selItem As Integer = lstIndex.SelectedIndex
                stripMenu_File_Save_Click(sender, e)
                lstIndex.SelectedIndex = selItem
                MapModified = False

            ElseIf response = DialogResult.No Then

                response = MessageBox.Show(
                    "You are about to lose progress, are you sure you wish to continue?",
                    "Warning: Loss of progress!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question)

                If response = DialogResult.No Then Exit Sub

            Else ' They chose cancel

                Exit Sub

            End If

        End If

        Audio.StopMusic()
        Map.Load(Path.Map & lstIndex.SelectedItem.ToString & ".map")
        RelistMaps()
        ResetPropertyWindow()

    End Sub

    Private Sub txtSearch_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSearch.KeyPress

        If e.KeyChar = Convert.ToChar(Keys.Enter) Then btnSearch_Click(sender, e)

    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged

        If txtSearch.Text = "" Then RelistMaps()

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        RelistMaps()

        For i As Integer = lstIndex.Items.Count - 1 To 0 Step -1

            If Not lstIndex.Items.Item(i).ToString.ToLower.Contains(txtSearch.Text.Trim.ToLower) Then
                lstIndex.Items.RemoveAt(i)
            End If

        Next

    End Sub

#End Region

#Region " Properties "

    Private Sub scrlSizeX_Scroll(sender As Object, e As ScrollEventArgs) Handles scrlSizeX.Scroll

        Map.Resize(scrlSizeX.Value, scrlSizeY.Value, scrlSizeZ.Value)
        MapModified = True
        ResetPropertyWindow()

    End Sub

    Private Sub scrlSizeY_Scroll(sender As Object, e As ScrollEventArgs) Handles scrlSizeY.Scroll

        Map.Resize(scrlSizeX.Value, scrlSizeY.Value, scrlSizeZ.Value)
        MapModified = True
        ResetPropertyWindow()

    End Sub

    Private Sub scrlSizeZ_Scroll(sender As Object, e As ScrollEventArgs) Handles scrlSizeZ.Scroll

        If scrlSizeZ.Value < Map.MaxZ Then

            Dim response As DialogResult = MessageBox.Show(
                "You are about to decrease the Z-Size of the map and lose all" &
                " content made on that layer. Do you wish to continue?",
                "Warning: Potential progress loss!",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning)

            If response = DialogResult.No Then Exit Sub

        End If

        Map.Resize(scrlSizeX.Value, scrlSizeY.Value, scrlSizeZ.Value)

        If scrlEditZ.Value > scrlSizeZ.Value Then
            scrlEditZ.Value = scrlSizeZ.Value
            EditorZ = CByte(scrlSizeZ.Value)
        End If

        MapModified = True
        ResetPropertyWindow()

    End Sub

    Private Sub scrlEditLayer_Scroll(sender As Object, e As ScrollEventArgs) Handles scrlEditLayer.Scroll

        EditorLayer = CType(scrlEditLayer.Value, LayerType)
        ResetPropertyWindow()

    End Sub

    Private Sub scrlEditZ_Scroll(sender As Object, e As ScrollEventArgs) Handles scrlEditZ.Scroll

        EditorZ = CByte(scrlEditZ.Value)
        ResetPropertyWindow()

    End Sub

    Private Sub btnShiftUp_Click(sender As Object, e As EventArgs) Handles btnShiftUp.Click

        Map.Shift(DirectionType.Up, EditorZ)
        MapModified = True

    End Sub

    Private Sub btnShiftLeft_Click(sender As Object, e As EventArgs) Handles btnShiftLeft.Click

        Map.Shift(DirectionType.Left, EditorZ)
        MapModified = True

    End Sub

    Private Sub btnShiftRight_Click(sender As Object, e As EventArgs) Handles btnShiftRight.Click

        Map.Shift(DirectionType.Right, EditorZ)
        MapModified = True

    End Sub

    Private Sub btnShiftDown_Click(sender As Object, e As EventArgs) Handles btnShiftDown.Click

        Map.Shift(DirectionType.Down, EditorZ)
        MapModified = True

    End Sub

    Private Sub btnProperties_Click(sender As Object, e As EventArgs) Handles btnProperties.Click

        frmProperties.Visible = Not frmProperties.Visible

    End Sub

#End Region

#Region " Map "

    Private Sub rsMap_Render() Handles rsMap.Render

        ' Dont attempt render when nothing will present
        If Graphics.Tileset Is Nothing OrElse
            Map.MaxX = 0 OrElse Map.MaxY = 0 Then Return

        ' Floor
        For y As Integer = 0 To Map.MaxY
            For x As Integer = 0 To Map.MaxX
                Graphics.Tileset(0).Position = New Vector2f(x * TileSize, y * TileSize)
                rsMap.Draw(Graphics.Tileset(0))
            Next
        Next

        ' Height
        For z As Integer = 0 To Map.MaxZ
            If z > EditorZ Then Exit For

            'Ground
            For l As Integer = LayerType.Ground To LayerType.Mask2
                For y As Integer = 0 To Map.MaxY
                    For x As Integer = 0 To Map.MaxX
                        Dim ts As Integer = Map.Layer(z).Tile(x, y)(l).Tileset
                        If ts < 1 Then Continue For

                        If z = EditorZ Then
                            If l <= EditorLayer Then
                                ' Normal Tileset
                                Graphics.Tileset(ts).TextureRect = New IntRect(Map.Layer(z).Tile(x, y)(l).X * TileSize,
                             Map.Layer(z).Tile(x, y)(l).Y * TileSize, TileSize, TileSize)
                                Graphics.Tileset(ts).Position = New Vector2f(x * TileSize, y * TileSize)
                                rsMap.Draw(Graphics.Tileset(ts))
                            Else
                                ' Transparency Tileset
                                Dim tmpTileset As New Sprite(Graphics.Tileset(ts).Texture)
                                tmpTileset.TextureRect = New IntRect(Map.Layer(z).Tile(x, y)(l).X * TileSize,
                             Map.Layer(z).Tile(x, y)(l).Y * TileSize, TileSize, TileSize)
                                tmpTileset.Position = New Vector2f(x * TileSize, y * TileSize)

                                ' Transparency Color
                                tmpTileset.Color = New Color(255, 255, 255, CByte(255 - 50 * l))
                                rsMap.Draw(tmpTileset)
                            End If
                        ElseIf z < EditorZ Then
                            ' Darker Tileset
                            Dim tmpTileset As New Sprite(Graphics.Tileset(ts).Texture)
                            tmpTileset.TextureRect = New IntRect(Map.Layer(z).Tile(x, y)(l).X * TileSize,
                         Map.Layer(z).Tile(x, y)(l).Y * TileSize, TileSize, TileSize)
                            tmpTileset.Position = New Vector2f(x * TileSize, y * TileSize)

                            ' Daker Color
                            tmpTileset.Color = New Color(128, 128, 128, 255)
                            rsMap.Draw(tmpTileset)
                        End If

                        ' Mouse Hover
                        If EditorMouseMapHover Then
                            If z = EditorZ AndAlso l = EditorLayer Then
                                rsMap_RenderMouseHover()
                            End If
                        End If
                    Next
                Next
            Next

            'Roof
            For l As Integer = LayerType.Fringe1 To LayerType.Count - 1
                For y As Integer = 0 To Map.MaxY
                    For x As Integer = 0 To Map.MaxX
                        Dim ts As Integer = Map.Layer(z).Tile(x, y)(l).Tileset
                        If ts < 1 Then Continue For

                        If z = EditorZ Then
                            If l <= EditorLayer Then
                                ' Normal Tileset
                                Graphics.Tileset(ts).TextureRect = New IntRect(Map.Layer(z).Tile(x, y)(l).X * TileSize,
                             Map.Layer(z).Tile(x, y)(l).Y * TileSize, TileSize, TileSize)
                                Graphics.Tileset(ts).Position = New Vector2f(x * TileSize, y * TileSize)
                                rsMap.Draw(Graphics.Tileset(ts))
                            Else
                                ' Transparency Tileset
                                Dim tmpTileset As New Sprite(Graphics.Tileset(ts).Texture)
                                tmpTileset.TextureRect = New IntRect(Map.Layer(z).Tile(x, y)(l).X * TileSize,
                             Map.Layer(z).Tile(x, y)(l).Y * TileSize, TileSize, TileSize)
                                tmpTileset.Position = New Vector2f(x * TileSize, y * TileSize)

                                ' Transparency Color
                                tmpTileset.Color = New Color(255, 255, 255, CByte(255 - 50 * l))
                                rsMap.Draw(tmpTileset)
                            End If
                        ElseIf z < EditorZ Then
                            ' Darker Tileset
                            Dim tmpTileset As New Sprite(Graphics.Tileset(ts).Texture)
                            tmpTileset.TextureRect = New IntRect(Map.Layer(z).Tile(x, y)(l).X * TileSize,
                         Map.Layer(z).Tile(x, y)(l).Y * TileSize, TileSize, TileSize)
                            tmpTileset.Position = New Vector2f(x * TileSize, y * TileSize)

                            ' Daker Color
                            tmpTileset.Color = New Color(128, 128, 128, 255)
                            rsMap.Draw(tmpTileset)
                        End If

                        ' Mouse Hover
                        If EditorMouseMapHover Then
                            If z = EditorZ AndAlso l = EditorLayer Then
                                rsMap_RenderMouseHover()
                            End If
                        End If
                    Next
                Next
            Next
        Next

    End Sub

    Private Sub rsMap_RenderMouseHover()

        ' Make clone of tileset for modding safely
        Dim tmpTileset As New Sprite(Graphics.Tileset(EditorTileset).Texture)
        tmpTileset.TextureRect = New IntRect(EditorX * TileSize, EditorY * TileSize, EditorSelectX * TileSize + TileSize, EditorSelectY * TileSize + TileSize)
        tmpTileset.Position = EditorMouseCoordinates
        tmpTileset.Color = Color.White
        rsMap.Draw(tmpTileset)

    End Sub

    Private Sub rsMap_MouseDown(sender As Object, e As MouseEventArgs) Handles rsMap.MouseDown

        EditorMouseDown = True

        Dim x As Integer = CInt(Math.Floor(e.X / TileSize))
        Dim y As Integer = CInt(Math.Floor(e.Y / TileSize))

        If e.Button = MouseButtons.Left Then
            SetTile(x, y)
        ElseIf e.Button = MouseButtons.Right Then
            ClearTile(x, y)
        End If

        MapModified = True

    End Sub

    Private Sub rsMap_MouseUp(sender As Object, e As MouseEventArgs) Handles rsMap.MouseUp

        EditorMouseDown = False

    End Sub

    Private Sub rsMap_MouseMove(sender As Object, e As MouseEventArgs) Handles rsMap.MouseMove

        Dim cX As Integer = CInt(Math.Floor(e.X / TileSize)) * TileSize
        Dim cY As Integer = CInt(Math.Floor(e.Y / TileSize)) * TileSize

        EditorMouseCoordinates = New Vector2f(cX, cY)

        If EditorMouseDown Then rsMap_MouseDown(sender, e)

    End Sub

    Private Sub rsMap_MouseEnter(sender As Object, e As EventArgs) Handles rsMap.MouseEnter

        EditorMouseMapHover = True

    End Sub

    Private Sub rsMap_MouseHover(sender As Object, e As EventArgs) Handles rsMap.MouseHover

        EditorMouseMapHover = True

    End Sub

    Private Sub rsMap_MouseLeave(sender As Object, e As EventArgs) Handles rsMap.MouseLeave

        EditorMouseMapHover = False

    End Sub

#End Region

#Region " Tileset "

    Private Sub rsTileset_Render() Handles rsTileset.Render

        ' Dont attempt render if no tileset exists
        If Graphics.Tileset Is Nothing Then Return

        ' Render Tileset
        Dim size As Vector2u = Graphics.Tileset(EditorTileset).Texture.Size
        Graphics.Tileset(EditorTileset).TextureRect = New IntRect(0, 0, CInt(size.X), CInt(size.Y))
        Graphics.Tileset(EditorTileset).Position = New Vector2f(0, 0)
        rsTileset.Draw(Graphics.Tileset(EditorTileset))

        ' Tile Selected
        Dim rect As New RectangleShape(New Vector2f(EditorSelectX * TileSize + TileSize, EditorSelectY * TileSize + TileSize))
        rect.Position = New Vector2f(EditorX * TileSize, EditorY * TileSize)
        rect.FillColor = Color.Transparent
        rect.OutlineColor = Color.Blue
        rect.OutlineThickness = 2
        rsTileset.Draw(rect)

        ' Mouse Hover
        If EditorMouseTilesetHover Then

            If EditorMouseDown Then

                rect = New RectangleShape(New Vector2f(EditorSelectX * TileSize + TileSize, EditorSelectY * TileSize + TileSize))
                rect.Position = New Vector2f(EditorX * TileSize, EditorY * TileSize)

            Else

                rect = New RectangleShape(New Vector2f(TileSize, TileSize))
                rect.Position = EditorMouseCoordinates

            End If

            rect.FillColor = Color.Transparent
            rect.OutlineColor = Color.Red
            rect.OutlineThickness = 2
            rsTileset.Draw(rect)

        End If

    End Sub

    Private Sub rsTileset_MouseDown(sender As Object, e As MouseEventArgs) Handles rsTileset.MouseDown

        If Not EditorMouseDown Then
            EditorX = CInt(Math.Floor(e.X / TileSize))
            EditorY = CInt(Math.Floor(e.Y / TileSize))
        End If

        EditorMouseDown = True

        Dim tsSize As Vector2u = Graphics.Tileset(EditorTileset).Texture.Size
        Dim maxX As Integer = CInt(tsSize.X / TileSize)
        Dim maxY As Integer = CInt(tsSize.Y / TileSize)

        EditorSelectX = CInt(Math.Floor(e.X / TileSize)) - EditorX
        EditorSelectY = CInt(Math.Floor(e.Y / TileSize)) - EditorY

        If EditorSelectX < 0 Then EditorSelectX = 0
        If EditorSelectY < 0 Then EditorSelectY = 0

        If EditorSelectX + EditorX > maxX Then EditorSelectX = maxX - EditorX
        If EditorSelectY + EditorY > maxY Then EditorSelectY = maxY - EditorY

    End Sub

    Private Sub rsTileset_MouseUp(sender As Object, e As MouseEventArgs) Handles rsTileset.MouseUp

        EditorMouseDown = False

    End Sub

    Private Sub rsTileset_MouseMove(sender As Object, e As MouseEventArgs) Handles rsTileset.MouseMove

        Dim cX As Integer = CInt(Math.Floor(e.X / TileSize)) * TileSize
        Dim cY As Integer = CInt(Math.Floor(e.Y / TileSize)) * TileSize

        EditorMouseCoordinates = New Vector2f(cX, cY)

        If EditorMouseDown Then rsTileset_MouseDown(sender, e)

    End Sub

    Private Sub rsTileset_MouseEnter(sender As Object, e As EventArgs) Handles rsTileset.MouseEnter

        EditorMouseTilesetHover = True

    End Sub

    Private Sub rsTileset_MouseHover(sender As Object, e As EventArgs) Handles rsTileset.MouseHover

        EditorMouseTilesetHover = True

    End Sub

    Private Sub rsTileset_MouseLeave(sender As Object, e As EventArgs) Handles rsTileset.MouseLeave

        EditorMouseTilesetHover = False

    End Sub

    Private Sub scrlTilesetChange_Scroll(sender As Object, e As ScrollEventArgs) Handles scrlTilesetChange.Scroll

        EditorTileset = scrlTilesetChange.Value
        EditorX = 0
        EditorY = 0

        pnlTileset.AutoScrollPosition = New Drawing.Point(0, 0)
        pnlTileset.Refresh()

        rsTileset.Width = Graphics.Tileset(EditorTileset).TextureRect.Width
        rsTileset.Height = Graphics.Tileset(EditorTileset).TextureRect.Height

    End Sub

#End Region

End Class