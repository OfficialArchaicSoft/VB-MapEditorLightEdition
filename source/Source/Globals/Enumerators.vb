﻿Friend Module modEnumerators

    Friend Enum DirectionType As Integer
        Up
        Left
        Right
        Down
    End Enum

    Friend Enum LayerType As Integer
        Ground
        Mask1
        Mask2
        Fringe1
        Fringe2
        Count
    End Enum

End Module