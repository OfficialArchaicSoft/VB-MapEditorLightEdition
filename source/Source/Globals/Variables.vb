﻿Friend Module modVariables
        
    ' Map Stuff    
    Friend Map As New MapDef(15, 15, 0)
    Friend MapModified As Boolean = False

    ' Editor values
    Friend EditorLayer As LayerType
    Friend EditorAutoTile As Integer
    Friend EditorTileset As Integer = 1
    Friend EditorX As Integer
    Friend EditorY As Integer
    Friend EditorZ As Integer
    Friend EditorSelectX As Integer
    Friend EditorSelectY As Integer
    Friend EditorMouseDown As Boolean
    Friend EditorMouseMapHover As Boolean
    Friend EditorMouseTilesetHover As Boolean
    Friend EditorMouseCoordinates As New SFML.System.Vector2f(0, 0)

End Module